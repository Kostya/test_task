﻿using System;
using System.Collections.Generic;

namespace Task_2
{
    class T2
    {
        const int input_size = 30;
        List<int> numbers = new List<int>();
        int summ;

        public void RandomInput()
        {
            Random r = new Random();

            for (int i = 0; i < input_size; i++)
            {
                int n = r.Next(100);

                numbers.Add(n);
                Console.Write("{0} ", n);
            }

            summ = r.Next(50, 150);
        }

        public List<Tuple<int, int>> SearchForPairs(List<int> nums, int s)
        {
            var _list = new List<Tuple<int, int>>();

            for (int i = 0; i < nums.Count - 1; i++)
                for (int j = i + 1; j < nums.Count; j++)
                    if (nums[i] + nums[j] == s)
                        _list.Add(new Tuple<int, int>(nums[i], nums[j]));

            return _list;
        }

        public void TestMe()
        {
            Console.WriteLine("Hi, {0}, this is a summ test\n", Environment.UserName);
            RandomInput();
            Console.WriteLine("\nthe summ is {0}\n", summ);

            var _pairs = SearchForPairs(numbers, summ);

            if (_pairs.Count == 0)
                Console.WriteLine("no pairs found");
            else
            {
                Console.WriteLine("{0} pair(s) found:\n", _pairs.Count);

                for (int i = 0; i < _pairs.Count; i++)
                    Console.WriteLine("{0}, {1}", _pairs[i].Item1, _pairs[i].Item2);
            }
        }
    }
}
