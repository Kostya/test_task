﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Task_1
{
    class T1
    {
        TSQueue<int> queue;

        public class TSQueue<T>
        {
            readonly object _lock = new object();
            Queue<T> _queue = new Queue<T>();

            public void Push(T o)
            {
                lock (_lock)
                {
                    _queue.Enqueue(o);

                    Monitor.PulseAll(_lock);
                }
            }

            public T Pop()
            {
                lock (_lock)
                {
                    while (_queue.Count == 0)
                        Monitor.Wait(_lock);

                    return _queue.Dequeue();
                }
            }
        }

        void PushProcess()
        {
            for (int i = 0; i < 10; i++)
            {
                queue.Push(i);
                Console.WriteLine("th_{0}: push {1}", Thread.CurrentThread.ManagedThreadId, i);
            }
        }

        void PopProcess()
        {
            for (int i = 0; i < 10; i++)
            {
                object o = queue.Pop();
                Console.WriteLine("th_{0}: pop  {1}", Thread.CurrentThread.ManagedThreadId, o);
            }
        }

        public void TestMe()
        {
            queue = new TSQueue<int>();

            Console.WriteLine("Hi, {0}, this is a thread-safe queue test\n", Environment.UserName);

            Task t1 = new Task(new Action(PopProcess));
            Task t2 = new Task(new Action(PushProcess));
            Task t3 = new Task(new Action(PushProcess));
            Task t4 = new Task(new Action(PopProcess));

            t1.Start();
            t2.Start();
            t3.Start();
            t4.Start();

            Task.WaitAll(t1, t2, t3, t4);
            Console.WriteLine("\ndone");
        }
    }
}
